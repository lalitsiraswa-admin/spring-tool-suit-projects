package com.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GreetController {
	@RequestMapping("/greet")
	public String greetMessage(Model model) {
		String data = "Have a good day";
		model.addAttribute("message", data);
		return "success";
	}

	@RequestMapping("/sayHello")
	public String sayHello(ModelMap map) {
		map.addAttribute("message", "Hello MVC");
		return "success";
	}

	@RequestMapping("welcome")
	public ModelAndView welcomMessage() {
		ModelAndView modelView = new ModelAndView("success", "message", "Welcome to MVC");
		return modelView;
	}
}
