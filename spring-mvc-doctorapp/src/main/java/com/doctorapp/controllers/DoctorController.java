package com.doctorapp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.doctorapp.model.Doctor;
import com.doctorapp.service.IDoctorService;

@Controller
public class DoctorController {
	// Inject Service to the controller
	@Autowired
	private IDoctorService doctorService;
	@RequestMapping("/")
	public String getAllDoctors(ModelMap map) {
		// call the service layer
		List<Doctor> doctors = doctorService.getAllDoctors();
		// attach the doctors to the mode map
		map.addAttribute("doctors", doctors);
		return "home";
	}
	@RequestMapping("/search")
	// use RequestParam to retrieve with parameters(name of the form field)
	// now the value of RequestParam is passed t the local variable
	public String searchDoctors(@RequestParam("specility")String special, ModelMap map) {
		List<Doctor> doctors = doctorService.getBySpeciality(special);
		map.addAttribute("doctors", doctors);
		return "home";
	}
	@RequestMapping("/admin")
	public String admin() {
		// move to the admin.jsp page
		return "adminDashBoard";
	}
	@RequestMapping("/addDoctorForm")
	public String showAddDoctorForm() {
		return "addForm";
	}
	@RequestMapping("/addDoctor")
	public String addDoctor(Doctor doctor) {
		// call the add doctor method of service layer
		System.out.println(doctor);
		doctorService.addDoctor(doctor);
		return "adminDashBoard";
	}
	
	@RequestMapping("/updateDoctorForm")
	public String showEditForm() {
		return "editForm";
	}
	@RequestMapping("/doctorById")
	public String getDoctorById(@RequestParam("doctorId") int doctorId, ModelMap map) {
		Doctor doctor = doctorService.getById(doctorId);
		map.addAttribute("doctor", doctor);
		return "updateForm";
	}
	@RequestMapping("/updateDoctor")
	public String updateDoctor(Doctor doctor) {
		// call the add doctor method of service layer
		System.out.println(doctor);
		doctorService.updateDoctor(doctor);
		return "adminDashBoard";
	}
	@RequestMapping("/deleteDoctorForm")
	public String showDeleteDoctorForm() {
		return "deleteForm";
	}
	@RequestMapping("/deletedoctorById")
	public String deleteDoctor(@RequestParam("doctorId")int doctorId) {
		// call the add doctor method of service layer
		doctorService.deleteDoctor(doctorId);
		return "adminDashBoard";
	}
	@RequestMapping("/home")
	public String home() {
		// use redirect to redirect to url not to a jsp page
//		return "redirect:/";
		return "redirect:/";
	}
}
