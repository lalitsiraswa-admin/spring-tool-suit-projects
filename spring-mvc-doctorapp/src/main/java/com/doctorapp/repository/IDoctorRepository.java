package com.doctorapp.repository;

import com.doctorapp.exceptions.DoctorNotFoundException;
import com.doctorapp.model.Doctor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import javax.print.Doc;
import java.util.List;

@Repository
public interface IDoctorRepository extends MongoRepository<Doctor, Integer> {
    // derieved queries - derieved from properties
    // readByPropertyName
    // findByPropertyName
    // getByPropertyName
    // instance variable name/ property name
    List<Doctor> findBySpeciality(String speciality); // all are similar(find, get, read)
    @Query("{fees:{$lte:?0}") // db.doctor.find("{fees:{$lte:1000}}")
    List<Doctor> getByFees(double fees);
    @Query("{experience:{$gte?0}}") // db.doctor.find("{experience:{$gte:10}}")
    List<Doctor> readByExperience(int experience);
    List<Doctor> findByHospitalName(String special);
    // db.doctor.find("{speciality:'cardio', fees:{$lte:1000}}")
    // db.doctor.find("{$and:[{speaciality:'cardio'}, {fees:{$lte:1000}}]}")
    @Query("{speciality:?0, fess:{$lte:?1}}")
    List<Doctor> getBySpecialityAndFees(String speciality, double fees);
    @Query("{$and:[{hospitalName:?0}, {spaciality:?1}]}")
    List<Doctor> findByHospitalNameAndSpeciality(String hospitalName, String speciality);
    @Query("{$and:[{experience:{$gte:?1}}, {speciality:?0}]}")
    List<Doctor> getBySpecialityAndExperience(String speciality, int exp)throws DoctorNotFoundException;


    @Query("{$or:[{experience:{$gte:?1}}, {speciality:?0}]}")
    List<Doctor> getBySpecialityOrExperience(String speciality, int exp)throws DoctorNotFoundException;
}
