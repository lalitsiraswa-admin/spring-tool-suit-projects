package com.patientapp.exceptions;

public class AadharNumberNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AadharNumberNotFoundException() {
		super();
	}
	public AadharNumberNotFoundException(String message) {
		super(message);
	}
}
