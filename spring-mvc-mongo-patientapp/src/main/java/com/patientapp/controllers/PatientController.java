package com.patientapp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.patientapp.model.Patient;
import com.patientapp.service.IPatientService;

@Controller
public class PatientController {
	// Inject service to the controller
	@Autowired
	private IPatientService patientService;
	
	@RequestMapping("/")
	public String getAllDoctors(ModelMap map) {		
		// call the method from service layer
		List<Patient> patients = patientService.getAllPatients();
		// attach the doctors to the model map
		map.addAttribute("patients", patients);
		return "home";
	}
	@RequestMapping("/search")
	public String searchPatient(@RequestParam("aadharnumber")String aadharNumber, ModelMap map) {
		Patient patients = patientService.getByAadharNumber(aadharNumber);
		map.addAttribute("patients", patients);
		return "home";
	}
	@RequestMapping("/admin")
	public String admin() {
		// move to the admin.jsp page
		return "adminDashboard";
	}
	@RequestMapping("/addPatientForm")
	public String showAddForm() {
		return "addForm";
	}
	@RequestMapping("/addPatient")
	public String addDoctor(Patient patient) {
		// call the addPatient method from the service layer
		patientService.addPatient(patient);
		return "redirect:/";
	}
	@RequestMapping("/updatePatientForm")
	public String showUpdateForm() {
		return "editForm";
	}
	@RequestMapping("/patientById")
	public String getPatientById(@RequestParam("aadharNumber")String aadharNumber, ModelMap map) {
		Patient patient = patientService.getByAadharNumber(aadharNumber);
		map.addAttribute("patient", patient);
		return "updateForm";
	}
	@RequestMapping("/updatePatient")
	public String updatePatient(Patient patient) {
		// call the update method from the service layer
		patientService.updatePatient(patient);
		return "adminDashboard";
	}
	@RequestMapping("/deletePatientForm")
	public String showDeleteForm() {
		return "deleteForm";
	}
	@RequestMapping("/deletePatient")
	public String deletePatient(@RequestParam("aadharNumber")String aadharNumber) {
		patientService.deletePatient(aadharNumber);
		return "adminDashboard";
	}
	@RequestMapping("/home")
	public String home() {
		// use "redirect:/" to redirect to an url not to an jsp page
		return "redirect:/";
	}
}
