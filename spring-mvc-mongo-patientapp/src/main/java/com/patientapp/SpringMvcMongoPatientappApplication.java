package com.patientapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcMongoPatientappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcMongoPatientappApplication.class, args);
	}

}
