package com.patientapp.model;

import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//@Getter
//@Setter
@NoArgsConstructor
//@AllArgsConstructor
//@ToString
//@Document
@Data
public class Patient {
	private String patientName;
	@Id
	private String aadharNumber;
	private Integer patientBedNo;
	private String underConsideration;
	private String disease;
	private String admitDate;
	private String releaseDate;
	private String contactNo;
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getAadharNumber() {
		return aadharNumber;
	}
	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	public Integer getPatientBedNo() {
		return patientBedNo;
	}
	public void setPatientBedNo(Integer patientBedNo) {
		this.patientBedNo = patientBedNo;
	}
	public String getUnderConsideration() {
		return underConsideration;
	}
	public void setUnderConsideration(String underConsideration) {
		this.underConsideration = underConsideration;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	public String getAdmitDate() {
		return admitDate;
	}
	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	@Override
	public String toString() {
		return "Patient [patientName=" + patientName + ", aadharNumber=" + aadharNumber + ", patientBedNo="
				+ patientBedNo + ", underConsideration=" + underConsideration + ", disease=" + disease + ", admitDate="
				+ admitDate + ", releaseDate=" + releaseDate + ", contactNo=" + contactNo + "]";
	}
	
}
