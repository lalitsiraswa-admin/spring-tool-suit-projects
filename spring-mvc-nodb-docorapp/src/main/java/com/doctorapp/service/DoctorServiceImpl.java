package com.doctorapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestTemplate;

import com.doctorapp.exceptions.DoctorNotFoundException;
import com.doctorapp.model.Doctor;

@Service
public class DoctorServiceImpl implements IDoctorService{
	@Autowired
	private RestTemplate restTemplate;
	private final String BASEURL = "http://localhost:8082/doctor-api/doctors";
	@Override
    public void addDoctor(Doctor doctor) {
		System.out.println(doctor);
		// create a map to store the values from a form
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("doctorId", doctor.getDoctorId());
		map.add("experience", doctor.getExperience());
		map.add("fees", doctor.getFees());
		map.add("speciality", doctor.getSpeciality());
		map.add("experience", doctor.getExperience());	
		map.add("name", doctor.getName());
		// Add a header for setting the media type
//		HttpHeaders headers = new HttpHeaders();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		// create the request
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);
		restTemplate.postForEntity(BASEURL, request, null, Void.class);
    }

    @Override
    public void updateDoctor(Doctor doctor) {
		System.out.println(doctor);
		// create a map to store the values from a form
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("doctorId", doctor.getDoctorId());
		map.add("experience", doctor.getExperience());
		map.add("fees", doctor.getFees());
		map.add("speciality", doctor.getSpeciality());
		map.add("experience", doctor.getExperience());	
		map.add("name", doctor.getName());
		// Add a header for setting the media type
//		HttpHeaders headers = new HttpHeaders();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		// create the request
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);
//		restTemplate.postForEntity(BASEURL, request, null, Void.class);
		// use exchange for calling the PutMapping in REST API
		restTemplate.exchange(BASEURL, HttpMethod.PUT, request, Void.class);
    }

    @Override
    public void deleteDoctor(int doctorId) {
    	String url = BASEURL+"/id/"+doctorId;
    	restTemplate.delete(url, doctorId);
    }

    @Override
    public Doctor getById(int doctorId) {
    	String url = BASEURL + "/id/"+doctorId;
        ResponseEntity<Doctor> responseEntity = restTemplate.getForEntity(url, Doctor.class);
        Doctor doctor = responseEntity.getBody();
        System.out.println("Status "+ responseEntity.getStatusCodeValue());
        return doctor;
    }

    @Override
    public List<Doctor> getAllDoctors() {
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(BASEURL, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getByFees(double fees) {
    	String url = BASEURL + "/fees/"+fees;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getBySpeciality(String speciality) {
    	String url = BASEURL + "/speciality/"+speciality;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getByExperience(int experience) {
    	String url = BASEURL + "/experience/"+experience;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getByHospitalName(String hospitalName) {
    	String url = BASEURL + "/hospitalName/"+hospitalName;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getByHospitalAndSpeciality(String hospitalName, String speciality) {
    	String url = BASEURL + "/hospitalName/"+hospitalName+"/speciality/"+speciality;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();
    }

    @Override
    public List<Doctor> getBySpecialityAndFees(String speciality, double fees) {
    	String url = BASEURL + "/speciality/"+speciality+"/fees/"+fees;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody();  
     }

    @Override
    public List<Doctor> getBySpecialityAndExp(String speciality, int exp) {
    	String url = BASEURL + "/speciality/"+speciality+"/experience/"+exp;
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);
        System.out.println(responseEntity.getHeaders().getFirst("info"));
        return responseEntity.getBody(); 
    }
    @ExceptionHandler(DoctorNotFoundException.class)
    public String handleException(DoctorNotFoundException e) {
    	return "home";
    }
}
