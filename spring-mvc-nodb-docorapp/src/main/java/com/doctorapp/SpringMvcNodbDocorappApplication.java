package com.doctorapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringMvcNodbDocorappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcNodbDocorappApplication.class, args);
	}
	// Spring IOC will create object of this class
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
